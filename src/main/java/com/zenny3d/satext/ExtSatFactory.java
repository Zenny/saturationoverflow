package com.zenny3d.satext;

import java.util.concurrent.Callable;

class ExtSatFactory implements Callable<IExtSat> {

	@Override
	public IExtSat call() throws Exception {
		return new ExtSat();
	}
}