package com.zenny3d.satext;

public class ExtSat implements IExtSat {

	private float saturation = 0.0f;
	
	@Override
	public float getExtSat() {
		return saturation;
	}

	@Override
	public void setExtSat(float i) {
		saturation = i;
	}

}
