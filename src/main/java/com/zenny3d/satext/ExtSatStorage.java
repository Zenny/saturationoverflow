package com.zenny3d.satext;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTPrimitive;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;

public class ExtSatStorage implements IStorage<IExtSat> {

	@Override
	public NBTBase writeNBT(Capability capability, IExtSat instance, EnumFacing side) {
		return new NBTTagFloat(instance.getExtSat());
	}

	@Override
	public void readNBT(Capability capability, IExtSat instance, EnumFacing side, NBTBase nbt) {
		instance.setExtSat(((NBTPrimitive)nbt).getFloat());
	}

}
