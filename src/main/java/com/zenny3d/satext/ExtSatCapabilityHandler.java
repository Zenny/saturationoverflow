package com.zenny3d.satext;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ExtSatCapabilityHandler {
	public static final ResourceLocation SAT_CAP = new ResourceLocation(SatExtMod.MODID, "ExtSat");
	
	@SubscribeEvent
	public static void addCapabilitiesEntity(AttachCapabilitiesEvent<Entity> e)
	{
		if (!(e.getObject() instanceof EntityLivingBase)) return;
				
		EntityLivingBase ent = (EntityLivingBase) e.getObject();
		if (ent instanceof EntityPlayer)
			e.addCapability(SAT_CAP, new ExtSatProvider());
	}
}
