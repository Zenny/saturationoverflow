package com.zenny3d.satext;

import java.io.File;

import org.apache.logging.log4j.Logger;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = SatExtMod.MODID, name = SatExtMod.NAME, version = SatExtMod.VERSION, acceptedMinecraftVersions = SatExtMod.MC_VERSION)
public class SatExtMod {
    public static final String MODID = "zenny3d.saturationoverflow";
    public static final String NAME = "SaturationOverflow";
    public static final String VERSION = "1.1.1";
    public static final String MC_VERSION = "[1.12.2]";
    public static boolean canGainPastSat = false;
    
    public static Logger logger;
 
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        CapabilityManager.INSTANCE.register(IExtSat.class, new ExtSatStorage(), new ExtSatFactory()); // Register capability factory
        MinecraftForge.EVENT_BUS.register(ExtSatCapabilityHandler.class);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
    	 File configFile = new File(Loader.instance().getConfigDir(), "SaturationOverflow.cfg");
    	 Configuration config = new Configuration(configFile);
         config.load();
         
         Property canGain = config.get("main", "canGainPastSaturation", false);
         canGainPastSat = canGain.getBoolean();
         if(config.hasChanged())
             config.save();
    }
}
