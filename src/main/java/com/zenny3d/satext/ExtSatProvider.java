package com.zenny3d.satext;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

public class ExtSatProvider implements ICapabilitySerializable<NBTBase> {
	@CapabilityInject(IExtSat.class)
	public static final Capability<IExtSat> SAT_CAP = null;
	
	private IExtSat instance = SAT_CAP.getDefaultInstance();

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return capability == SAT_CAP;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		return capability == SAT_CAP ? SAT_CAP.<T> cast(this.instance) : null;
	}

	@Override
	public NBTBase serializeNBT() {
		return SAT_CAP.getStorage().writeNBT(SAT_CAP, instance, null);
	}

	@Override
	public void deserializeNBT(NBTBase nbt) {
		SAT_CAP.getStorage().readNBT(SAT_CAP, instance, null, nbt);
		
	}
}
