package com.zenny3d.satext;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.util.FoodStats;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(modid = SatExtMod.MODID)
public class SatExtEventHandler {

	private static final Map<UUID, Float> lastSaturationLevels = new HashMap<UUID, Float>();
	
	@SubscribeEvent
	public static void Start(LivingEntityUseItemEvent.Start evt) {
		if(evt.isCanceled() || !(evt.getEntity() instanceof EntityPlayer)) return;
		
		EntityPlayer player = (EntityPlayer)evt.getEntity();
		if(FMLCommonHandler.instance().getSide() == Side.SERVER || (FMLCommonHandler.instance().getSide() == Side.CLIENT && !player.world.isRemote)) {
			
			if(lastSaturationLevels.containsKey(player.getUniqueID()))
				lastSaturationLevels.replace(player.getUniqueID(), player.getFoodStats().getSaturationLevel());
			else
				lastSaturationLevels.put(player.getUniqueID(), player.getFoodStats().getSaturationLevel());
	
			//SatExtMod.logger.info("EATING");
		}
	}
	

	@SubscribeEvent
	public static void Stop(LivingEntityUseItemEvent.Stop evt) {
		if(evt.isCanceled() || !(evt.getEntity() instanceof EntityPlayer)) return;
		
		EntityPlayer player = (EntityPlayer)evt.getEntity();
		if(FMLCommonHandler.instance().getSide() == Side.SERVER || (FMLCommonHandler.instance().getSide() == Side.CLIENT && !player.world.isRemote)) {
			
			if(lastSaturationLevels.containsKey(player.getUniqueID())) {
				lastSaturationLevels.remove(player.getUniqueID());
				//SatExtMod.logger.info("STOP REM");
				
			}
		}
	}
	
	@SubscribeEvent
	public static void Finish(LivingEntityUseItemEvent.Finish evt) {
		if(evt.isCanceled() || !(evt.getItem().getItem() instanceof ItemFood) || !(evt.getEntity() instanceof EntityPlayer)) return;

		ItemFood food = (ItemFood)evt.getItem().getItem();
		EntityPlayer player = (EntityPlayer)evt.getEntity();
		if(FMLCommonHandler.instance().getSide() == Side.SERVER || (FMLCommonHandler.instance().getSide() == Side.CLIENT && !player.world.isRemote)) {
			IExtSat sat = player.getCapability(ExtSatProvider.SAT_CAP, null);
			if((sat.getExtSat() <= 0.0001 || SatExtMod.canGainPastSat) && lastSaturationLevels.containsKey(player.getUniqueID())) { // Sometimes the finish event fires twice?
				float foodSat = food.getHealAmount(evt.getItem()) * food.getSaturationModifier(evt.getItem());
				float addedSat = Math.min(20-lastSaturationLevels.get(player.getUniqueID()), foodSat);
				if(sat.getExtSat() < 0) sat.setExtSat(0);
				if(SatExtMod.canGainPastSat) 
					sat.setExtSat(sat.getExtSat() + (foodSat - addedSat));
				else
					sat.setExtSat(foodSat - addedSat); // Prevent the weird double-eating bug from doing anything just in case.
				//SatExtMod.logger.info("ATE: ext: "+(foodSat - addedSat) + " | " + lastSaturationLevels.size());
			}
			if(lastSaturationLevels.containsKey(player.getUniqueID())) {
				lastSaturationLevels.remove(player.getUniqueID());

				//SatExtMod.logger.info("FIN REM");
			}
			SatExtMod.logger.info("extsat: "+sat.getExtSat());
		}
	}
	
	public static Field saturationLevel = null;
	
	@SubscribeEvent
	public static void onPlayerTickEvent(PlayerTickEvent evt)
	{
		if(evt.isCanceled()) return;
		
		EntityPlayer player = evt.player;
		if(FMLCommonHandler.instance().getSide() == Side.SERVER || (FMLCommonHandler.instance().getSide() == Side.CLIENT && !player.world.isRemote)) {
			FoodStats pf = player.getFoodStats();
			float curSat = pf.getSaturationLevel();
			float needed = 20.0f - curSat;
			if(needed > 0.0) {
				IExtSat sat = player.getCapability(ExtSatProvider.SAT_CAP, null);
				if(sat.getExtSat() > 0.0f) {
					float toAdd = Math.min(needed, sat.getExtSat());
					//SatExtMod.logger.info(pf.getSaturationLevel()+"/20.0 Needed: "+needed + " Adding: "+toAdd);
					if(saturationLevel == null) {
						try {
							saturationLevel = ObfuscationReflectionHelper.findField(FoodStats.class, "field_75125_b");
						} catch(java.lang.NoSuchMethodError e) { // Potential fix for applecore, it deobfs the field name?
							saturationLevel = ObfuscationReflectionHelper.findField(FoodStats.class, "foodSaturationLevel");
						} catch (java.lang.NoSuchFieldError e) { // Potential fix for applecore, it deobfs the field name?
							saturationLevel = ObfuscationReflectionHelper.findField(FoodStats.class, "foodSaturationLevel");
						}
					}
					try {
						saturationLevel.set(pf, curSat + toAdd);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					// Decrease oversat
					sat.setExtSat(sat.getExtSat()-toAdd);
				}
			}
		}
	}
}
